import React from 'react';
import Grid from '@material-ui/core/Grid';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'ramda';
import { getUser } from '../../selectors/user';
import Header from '../Header';
import Body from '../Body';

const App = ({ user }) => {
  return (
    <Grid container>
      <Header user={user} />
      <Body />
    </Grid>
  );
}
const mapStateToProps = createStructuredSelector({
  user: getUser
});
export const enhance = compose(connect(mapStateToProps, null))
export default enhance(App);
