import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import { initials, fullname } from '../../models';
const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: 400,
        boxShadow: 'unset',
    },
   
    actions: {
        display: 'flex',
    },
    avatar: {
        backgroundColor: '#346c34'
    }
}));

const Component = ({ user }) => {
    const classes = useStyles();
    return (
        <Card className={classes.card}>
            <CardHeader avatar={<Avatar className={classes.avatar} >{initials(user)}</Avatar>}
                title={fullname(user)}
                subheader={user.email} />
            <CardActions className={classes.actions}>
                <Button
                    size="small"
                    color="primary"
                    onClick={() => {
                        //handleClose();
                        //logout();
                    }}
                >
                    Déconnexion
                </Button>
                <Button
                    size="small"
                    color="primary"

                >
                    voir votre profile
                </Button>
            </CardActions>
        </Card>
    )
}

export default Component;