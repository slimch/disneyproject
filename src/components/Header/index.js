import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Popover from '@material-ui/core/Popover';
import UserDetails from './userDetails';
import logo from '../../images/disney.png';
import { initials } from '../../models';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        position:'relative'
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        // flexGrow: 1,
        width: '100px'
    },
    avatar: {
        backgroundColor: '#346c34',
        position: 'absolute',
        right: 10
    }
}));

const Component = ({ user }) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleMenu = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Grid item xs={12}>
            <Grid container className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton disabled edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                            <MenuIcon />
                        </IconButton>
                        <img src={logo} width="100px" alt="logo Disney" />
                        <Avatar onClick={handleMenu} className={classes.avatar}>{initials(user)}</Avatar>

                        <Popover
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            open={open}
                            onClose={handleClose}
                            getContentAnchorEl={null}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                        >
                            <UserDetails user={user} />
                        </Popover>
                    </Toolbar>
                </AppBar>
            </Grid>
        </Grid>
    )
}


export default Component;