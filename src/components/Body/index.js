import React from 'react';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'ramda';
import Grid from '@material-ui/core/Grid';
import HotelCard from './Hotels/HotelCard';
import { getHotels } from '../../selectors/hotels';
import Panier from './Panier';
import * as panierActions from '../../actions/panier';
import { getBag, getTotalPrice } from '../../selectors/panier';


const Component = ({ hotels, actions, panier, totalPrice }) => {

    return (
        <Grid item xs={12}>
            <Grid container>
                <Grid item xs={1} />
                <Grid item xs={7}>
                    {hotels.map(element =>
                        <HotelCard key={element.id} element={element} actions={actions} />
                    )}
                </Grid>
                <Grid item xs={4}>
                    <Panier panier={panier} actions={actions} totalPrice={totalPrice} />
                </Grid>
            </Grid>
        </Grid>
    );
}
const actions = {
    addToBag: panierActions.addToBag,
    updateNumberOfNights: panierActions.updateNumberOfNights,
    clearBag: panierActions.clearBag,
    removeHotel: panierActions.removeHotel
};
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
});
const mapStateToProps = createStructuredSelector({
    hotels: getHotels,
    panier: getBag,
    totalPrice: getTotalPrice
});
export const enhance = compose(connect(mapStateToProps, mapDispatchToProps))
export default enhance(Component)