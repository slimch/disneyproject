import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Rating from '@material-ui/lab/Rating';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
    card: {
        display: 'flex',
        marginTop: theme.spacing(3)

    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    margin: {
        alignText: "center"
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 300,
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    playIcon: {
        height: 38,
        width: 38,
    },
}));

const Component = ({ element, actions }) => {
    const classes = useStyles();

    return (
        <Card className={classes.card}>
            <CardMedia
                className={classes.cover}
                image={require(`../../../images/${element.path}`)}
            // title="Live from space album cover"
            />

            <Grid container>
                <Grid item xs={10}>
                    <Grid container className={classes.details}>
                        <CardContent className={classes.content}>
                            <Typography component="h2" variant="h5">
                                {`${element.label} -- ${element.place}`}
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary">
                                {element.country}
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary">
                                {element.description}
                            </Typography>
                        </CardContent>
                    </Grid>
                </Grid>
                <Grid item xs={2}>
                    <Grid container justify="center" >
                        <div className={classes.details}>
                            <Typography component="h2" variant="h5">
                                {`${element.price}€`}
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary">
                                prix/nuit
                                </Typography>
                        </div>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container >
                        <Grid item xs={10}>
                            <Grid container className={classes.controls}>
                                <Rating value={element.rating} readOnly />
                            </Grid>
                        </Grid>
                        <Grid item xs={2} >
                            <Grid container justify="center" className={classes.controls}>
                                <Button onClick={() => actions.addToBag(element)} variant="contained" style={{ backgroundColor: "black", color: "white", fontWeight: 'bold' }} className={classes.button}>
                                    Ajouter
                                 </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>


        </Card>
    );
}
export default Component