import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Form, Field } from 'react-final-form';
import TextField from '../../../forms/TextField';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles';
import DisplayTotal from '../../../widgets/DisplayTotal';
import Success from '../../../widgets/Succes';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
    },
    bottom: {
        marginTop: theme.spacing(2)
    },
    button: {
        backgroundColor: "black",
        color: "white",
        fontWeight: 'bold',
        '&disable': {
            backgroundColor: 'red'
        }
    },

}));

const Component = ({ handleClose, open, panier, totalPrice, actions }) => {

    const classes = useStyles();
    const [confirmed, validateCommand] = React.useState(false);
    const [approuved, approuveCommand] = React.useState(false);
    const onSubmit = values => {
        validateCommand(true)
    };
    const validate = values => {
        const errors = {};
        if (!values.email) {
            errors.title = 'Required';
        }

        return errors;
    };
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            scroll="paper"
            aria-labelledby="scroll-dialog-title"
        >
            <Form
                onSubmit={onSubmit}
                validate={validate}
                render={({ handleSubmit, pristine, form, invalid, values }) => (
                    <form onSubmit={handleSubmit}>
                        <DialogTitle id="scroll-dialog-title">{!approuved ? !confirmed ? 'Informations personelles' : 'Récapitulatif de la réservation' : 'Succés'}</DialogTitle>
                        {!approuved ? <DialogContent dividers>
                            {!confirmed ?
                                <Grid container>
                                    <Field
                                        component={TextField}
                                        name="firstname"
                                        type="text"
                                        placeholder="Nom"
                                        fullWidth
                                        autoFocus={true}
                                        margin="dense"
                                    />
                                    <Field
                                        component={TextField}
                                        name="lastname"
                                        type="text"
                                        placeholder="Prenom"
                                        fullWidth
                                        margin="dense"
                                    />
                                    <Field
                                        component={TextField}
                                        name="email"
                                        type="email"
                                        placeholder="Email"
                                        fullWidth
                                        margin="dense"
                                    />
                                    <Field
                                        component={TextField}
                                        name="adresse"
                                        type="text"
                                        placeholder="Adresse"
                                        fullWidth
                                        margin="dense"
                                    />
                                </Grid> :
                                <Fragment>
                                    {panier.map(element =>
                                        <Paper key={element.id} className={classes.root} >
                                            <Grid container>
                                                <Grid item xs={3}>
                                                    <img src={require(`../../../images/${element.path}`)} width="100px" alt="elementImage" />
                                                </Grid>
                                                <Grid item xs={9}>
                                                    <Typography variant="subtitle2" >
                                                        {`${element.numberOfnights} nuitée(s) à ${element.label} --${element.country} à un prix total de ${element.finalPrice}€`}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Paper>
                                    )}
                                    <Grid item xs={12}>
                                        <Grid container justify="center" className={classes.bottom} >
                                            <DisplayTotal totalPrice={totalPrice} />
                                        </Grid>
                                    </Grid>
                                </Fragment>
                            }
                        </DialogContent> :
                            <DialogContent dividers>
                                <Success />
                            </DialogContent>}
                        <DialogActions>
                            {!approuved ? <Fragment>
                                <Button onClick={() => { handleClose(); approuveCommand(false); validateCommand(false) }} variant="contained" color="primary">
                                    Anuller
                            </Button>
                                {!confirmed ? <Button variant="contained" className={classes.button} disabled={pristine || invalid} type="submit" >
                                    Suivant
                            </Button> :
                                    <Button onClick={() => approuveCommand(true)} variant="contained" className={classes.button} color="primary">
                                        Finaliser
                        </Button>

                                }
                            </Fragment> :
                                <Button onClick={() => { actions.clearBag(); handleClose(); approuveCommand(false); validateCommand(false) }} variant="contained" className={classes.button} color="primary">
                                    Quitter
                                </Button>
                            }
                        </DialogActions>
                    </form>
                )}
            />
        </Dialog>
    )
}

export default Component;