import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Article from './Article';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import ConfirmationDialog from './ConfirmationDialog'
import DisplayTotal from '../../../widgets/DisplayTotal';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(3),
        width: '75%',
        position: 'relative'
    },
    title: {
        margin: theme.spacing(3)
    },
    card: {
        display: 'flex',
        marginTop: theme.spacing(2),
        width: '90%',
        height: '200px'
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    playIcon: {
        height: 38,
        width: 38,
    },
    bottom: {
        marginBottom: theme.spacing(2)
    },
    divider: {
        height: '4px',
        backgroundColor: 'rgba(0, 0, 0, 0.49)'
    },
    bottomTypography: {
        fontWeight: 'bold',
    },
    priceTypography: {
        color: '#2076d2'
    },
    button: {
        backgroundColor: "black",
        color: "white",
        fontWeight: 'bold',
        marginTop: theme.spacing(2),
        '&disable': {
            backgroundColor: 'red'
        }
    }
}));


const Component = ({ panier, actions, totalPrice }) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const handleClick = () => {
        setOpen(!open);
    };
    return (
        <Grid container justify="center">
            <Paper className={classes.root}>
                <Grid item xs={12}>
                    <Grid container justify="flex-start" className={classes.title}>
                        <Typography variant="h5" component="h5">
                            Total du panier
                    </Typography>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container style={{ marginBottom: 50 }}>
                        {panier && panier[0] ? panier.map(element =>
                            <Article key={element.id} element={element} actions={actions} />) :
                            <Grid container justify="center">
                                <Typography component="p">
                                    VOTRE PANIER NE COMPORTE AUCUN ARTICLE .
              </Typography>
                            </Grid>
                        }
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container justify="center" className={classes.bottom} >
                        <DisplayTotal totalPrice={totalPrice} />
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container justify="center" className={classes.bottom} >

                        <Grid item xs={10}>
                            <Divider className={classes.divider} />
                        </Grid>
                        <Grid item xs={6} />
                        <Grid item xs={6}>
                            <Grid container justify="center" >
                                <Button onClick={handleClick} disabled={totalPrice === 0 ? true : false} variant="contained" className={classes.button}>
                                    passer la commande
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>

            </Paper>
            <ConfirmationDialog open={open} handleClose={handleClick} panier={panier} totalPrice={totalPrice} actions={actions} />
        </Grid>
    );
}
export default Component