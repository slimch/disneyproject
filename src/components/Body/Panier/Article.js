import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import ShoppingCart from '@material-ui/icons/ShoppingCart';
import DeleteIcon from '@material-ui/icons/Delete';
const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(3),
        minHeight: 400,
        width: '80%'
    },
    title: {
        margin: theme.spacing(3)
    },
    card: {
        display: 'flex',
        marginTop: theme.spacing(2),
        width: '90%',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 200,
    },
    controls: {
        display: 'flex',
        alignItems: 'flex-end',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    playIcon: {
        height: 38,
        width: 38,
    },
    button: {
        backgroundColor: "black",
        color: "white",
        fontWeight: 'bold',
        marginLeft: theme.spacing(2),
        '&disable': {
            backgroundColor: 'red'
        }
    }
}));

const Component = ({ element, actions }) => {
    const classes = useStyles();
    const [values, setValues] = React.useState(1);

    const handleChange = id => event => {
        setValues(event.target.value);
    };
    return (


        <Grid container justify="center">
            <Card className={classes.card}>
                <CardMedia
                    className={classes.cover}
                    image={require(`../../../images/${element.path}`)}
                    title="Live from space album cover"
                />
                <Grid container>
                    <Grid item xs={12}>
                        <Grid container className={classes.details}>
                            <CardContent className={classes.content}>
                                <Grid item xs={12}>
                                    <Grid container direction="row">
                                        <Grid item xs={10}>
                                            <Typography component="h5" variant="h5">
                                                {element.label}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={2}>
                                            <IconButton onClick={() => actions.removeHotel(element.id)} aria-label="Settings">
                                                <DeleteIcon />
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Typography variant="subtitle1" color="textSecondary">
                                    {element.country}
                                </Typography>
                            </CardContent>
                        </Grid>
                    </Grid>

                    <Grid item xs={12}>
                        <Grid container >
                            <Grid item xs={8}>
                                <Grid container className={classes.controls}>
                                    <TextField
                                        style={{ width: 40 }}
                                        id="standard-number"
                                        value={values}
                                        onChange={handleChange(element.id)}
                                        type="number"
                                        className={classes.textField}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        InputProps={{
                                            inputProps: {
                                                min: 1,

                                            }
                                        }}
                                        margin="normal"
                                    />
                                    <Fab disabled={values < 1 ? true : false} onClick={() => actions.updateNumberOfNights({ 'id': element.id, 'value': values })} size="small" aria-label="add" className={classes.button}>
                                        <ShoppingCart />
                                    </Fab>
                                </Grid>
                            </Grid>
                            <Grid item xs={3} >
                                <Grid container justify="center" className={classes.controls}>
                                    <Typography component="h2" variant="h5">
                                        {`${element.finalPrice}€`}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Card>
        </Grid>


    );
}
export default Component