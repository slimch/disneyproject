
export const ADD_TO_BAG = "ADD_TO_BAG"
export const addToBag = (object) => ({ type: ADD_TO_BAG, object })

export const UPDATE_NUMBER_OF_NIGHTS = "UPDATE_NUMBER_OF_NIGHTS";
export const updateNumberOfNights = (object) => ({ type: UPDATE_NUMBER_OF_NIGHTS, object })

export const CLEAR_BAG = "CLEAR_BAG"
export const clearBag = () => ({ type: CLEAR_BAG });

export const REMOVE_HOTEL_FROM_BAG = "REMOVE_HOTEL_FROM_BAG";
export const removeHotel = (id) => ({ type: REMOVE_HOTEL_FROM_BAG, id })