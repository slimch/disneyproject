import { path, compose, map, sum } from 'ramda';
import { createSelector } from 'reselect';

export const getBag = path(['panier', 'panier']);
export const getTotalPrice = createSelector(getBag, bag => bag && compose(sum, map(element => element.finalPrice))(bag))