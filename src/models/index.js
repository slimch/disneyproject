import { pick, take, compose, map, join,values,toUpper } from 'ramda';


export const initials = compose(  toUpper,join(''), values, map(take(1)), pick(['firstname', 'lastname']))
export const fullname=compose(join(' '),values,pick(['firstname','lastname']))