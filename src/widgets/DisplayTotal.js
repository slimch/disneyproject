import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';

const useStyles = makeStyles(theme => ({


    bottomTypography: {
        fontWeight: 'bold',
    },
    priceTypography: {
        color: '#2076d2'
    },

}));
const Component = ({ totalPrice }) => {
    const classes = useStyles();

    return (

        <Grid container>
            <Grid item xs={4}>
                <Grid container justify="center">
                    <Typography variant="h6" className={classes.bottomTypography}>
                        TOTAL
                    </Typography>
                </Grid>
            </Grid>
            <Grid item xs={4} />
            <Grid item xs={4}>
                <Grid container justify="center">
                    <Typography variant="h5" className={classNames(classes.bottomTypography, classes.priceTypography)}>
                        {`${totalPrice}€`}
                    </Typography>
                </Grid>
            </Grid>
        </Grid>

    )
}

export default Component;