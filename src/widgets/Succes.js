import React from 'react';
import Grid from '@material-ui/core/Grid';
import CheckCircle from '@material-ui/icons/CheckCircle';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
    icon: {
        color: 'green',
        fontSize: 60
    },


}));
const Component = () => {
    const classes = useStyles();

    return (
        <Grid container justify="center">
            <Grid item xs={12}>
                <Grid container justify="center">
                    <CheckCircle className={classes.icon} />

                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Grid container justify="center">
                    <Typography variant="h6">
                        Votre réservation a été validé avec succés
                    </Typography>
                    <Typography variant="h6">
                        Merci de votre confiance
                    </Typography>

                </Grid>
            </Grid>
        </Grid>
    )
}

export default Component;