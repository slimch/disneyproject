import { ADD_TO_BAG, UPDATE_NUMBER_OF_NIGHTS, CLEAR_BAG, REMOVE_HOTEL_FROM_BAG } from "../actions/panier";
import { append, reject, propEq, map } from 'ramda';
import faker from 'faker';

export default (state = { panier: [] }, action) => {
    switch (action.type) {
        case ADD_TO_BAG: {
            return {
                ...state,
                panier: append({
                    'id': faker.random.uuid(),
                    'label': action.object.label,
                    'country': action.object.country,
                    'price': action.object.price,
                    'path': action.object.path,
                    'numberOfnights': 1,
                    'finalPrice': action.object.price
                }, state.panier),
            };
        }
        case UPDATE_NUMBER_OF_NIGHTS: {
            return {
                ...state, panier: map(element => {
                    if (propEq('id', action.object.id)(element)) {
                        return { ...element, numberOfnights: action.object.value, finalPrice: element.price * action.object.value };
                    }
                    return element;
                })(state.panier)
            }
        }
        case CLEAR_BAG: {
            return { ...state, panier: [] }
        }
        case REMOVE_HOTEL_FROM_BAG: {
            return { ...state, panier: reject(propEq('id', action.id))(state.panier) };
        }
        default:
            return state;
    }
};