import { combineReducers } from 'redux';
import user from './user';
import hotels from './hotels';
import panier from './panier';

const createRootReducer = () =>
    combineReducers({
        user,
        hotels,
        panier
    });
export default createRootReducer;