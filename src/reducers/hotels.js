
import hotels from '../data/hotels';

export default (state = { hotels: hotels }, action) => {
    switch (action.type) {

        default:
            return state;
    }
};