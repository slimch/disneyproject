import React from 'react';
import TextField from '@material-ui/core/TextField';


const TextFieldForm = ({
  input: { value, onChange, name, ...restInput },
  meta: { error, touched },
  classes,
  ...rest
}) => (
  <TextField
    {...rest}
    name={name}
    value={value}
    onChange={onChange}
    inputProps={restInput}
    error={error && touched}
    helperText={touched ? error : undefined}
  />
);


export default TextFieldForm;
